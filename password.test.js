const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be true', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit in password', () => {
    expect(checkDigit('BBB1AAA2')).toBe(true)
  })
  test('should has not digit in password', () => {
    expect(checkDigit('ABCDEFG')).toBe(false)
  })
  test('should has 0-9 in password', () => {
    expect(checkDigit('0123456789')).toBe(true)
  })
  test('should has 9-0 in password', () => {
    expect(checkDigit('9876543210')).toBe(true)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password', () => {
    expect(checkSymbol('111!111')).toBe(true)
  })
  test('should has not symbol in password', () => {
    expect(checkSymbol('abcede')).toBe(false)
  })
  test('should has not symbol @ in password', () => {
    expect(checkSymbol('abcede')).toBe(false)
  })
  test('should has symbol @ in password', () => {
    expect(checkSymbol('@abcede')).toBe(true)
  })
})

describe('Test Password', () => {
  test('should password @Poom1234 to be true', () => {
    expect(checkPassword('@Poom1234')).toBe(true)
  })
  test('should password @Poom12 to be false', () => {
    expect(checkPassword('@Poom12')).toBe(false)
  })
  test('should password @1234567 to be false', () => {
    expect(checkPassword('@1234567')).toBe(false)
  })
  test('should password @ZZKK7666 to be true', () => {
    expect(checkPassword('@ZZKK7666')).toBe(true)
  })
  test('should password LnwPoomZa007 to be false', () => {
    expect(checkPassword('LnwPoomZa007')).toBe(false)
  })
  test('should password @LnwPoomZa to be false', () => {
    expect(checkPassword('@LnwPoomZa')).toBe(false)
  })
  test('should password @0947202 to be false', () => {
    expect(checkPassword('@0947202')).toBe(false)
  })
  test('should password 1234567 to be false', () => {
    expect(checkPassword('1234567')).toBe(false)
  })
})
