const checkLength = function (password) {
  return password.length >= 8 && password.length < 25
}

const checkAlphabet = function (password) {
  // const alphabets = 'abcdefghijklmnopqrstuvwxyz'
  // for (const ch of password) {
  //   if (alphabets.includes(ch.toLowerCase())) return true
  // }
  // return false
  return /[a-zA-Z]/.test(password)
}

const checkDigit = function (password) {
  return /[0-9]/.test(password)
}

const checkSymbol = function (password) {
  const symbol = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (symbol.includes(ch)) return true
  }
  return false
}

const checkPassword = function (password) {
  return checkLength(password) && checkAlphabet(password) && checkDigit(password) && checkLength(password) && checkSymbol(password)
}

module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
